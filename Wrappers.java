public class Wrappers {

    public static void main(String[] args){

        Short wrapper1  = 8;
        Byte wrapper2 = 28;
        Integer wrapper3 = 128;
        Long wrapper4 = 12800L;
        Float wrapper5 = 3.5F;
        Double wrapper6 = 3.55555D;
        Boolean wrapper7 = false;
        Character wrapper8 = 'G';

        System.out.println("Impressão dos valores:");
        System.out.println(" ");
        System.out.println("Short Wrapper 1: " +wrapper1);
        System.out.println("Byte Wrapper 2: " +wrapper2);
        System.out.println("Integer Wrapper 3: " +wrapper3);
        System.out.println("Long Wrapper 4: " +wrapper4);
        System.out.println("Float Wrapper 5: " +wrapper5);
        System.out.println("Double Wrapper 6: " +wrapper6);
        System.out.println("Boolean Wrapper 7: " +wrapper7);
        System.out.println("Character Wrapper 8: " + wrapper8 + "abriel");
    }
}
